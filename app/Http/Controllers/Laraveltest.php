<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Laraveltest extends Controller
{
    
    public function create()
    {
        return view('laravel_test');
    }

    public function save(Request $req){
        $salary_data = new \App\laravel_test;
        $data = $req->get('data');
        $title = $req->get('title');
        $basic = 10000;
        $hra = 2000;
        $da = 1500;
        $title_dt = explode(',',$title);
        array_shift($data);
        for($i =0;$i<count($data);$i++){
            if($data[$i]!=''){
                $dt[$i] = explode(',',$data[$i]);
                $data_post[]=$dt[$i];
            }
        }
        foreach($data_post as $key => $value){
            $months = config('custom_config.month');
            $months_no = intval($months[$value[2]]);
            $d=cal_days_in_month(CAL_GREGORIAN,$months_no,intval($value[3]));
            $basic = $basic * $value[1] / $d;
            $basic = round($basic,2);
            $hra = $hra * $value[1] /$d;
            $hra = round($hra,2);
            $da = $da * $value[1] /$d;
            $da = round($da,2);
            //$salary = $basic + $hra +$da;
            $salary_dt = array();
            $salary_dt['user_id'] = $value[0] ;
            $salary_dt['month'] = $value[2] ;
            $salary_dt['year'] =    $value[3] ;
            $salary_dt['DA'] = $da;
            $salary_dt['basic'] = $basic;
            $salary_dt['HRA'] = $hra;
            $id[] = $salary_data->save_data($salary_dt);
            
        }
        if(count($id) == count($data_post) && !empty($id)){
                return ['status'=>'success','message'=>$id];
            }else{
                return ['status'=>'error','message'=>'The data cannot be saved'];
            }
    }
}
