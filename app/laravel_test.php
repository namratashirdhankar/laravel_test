<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class laravel_test extends Model
{	
	protected $connection = 'mysql';
    protected $table = 'salary';
    protected $fillable = [
        'HRA',
        'basic',
        'DA',
        'month',
        'user_id',
        'year'
    ];

    public function save_data($data) {
		$salary_data = array();
        $salary_data['user_id'] = $data['user_id'];
        $salary_data['month'] = $data['month'];
        $salary_data['year'] = $data['year'];
        $salary_data['DA'] = $data['DA'];
        $salary_data['basic'] = $data['basic'];
        $salary_data['HRA'] = $data['HRA'];
        $id = $this->create($salary_data)->id;
		return $id;
    }
}
