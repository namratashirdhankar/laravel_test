<html>
<head>
</head>
<title>Laravel Test</title>
<body>
	<h3>Laravel Test</h3>
	<div class="main">
        <input type="hidden" value="{!! csrf_token() !!}" name="_token" id="_token" />                      
		<div class = "msg_class"></div>
		<input type='file' id='upload_csv'>
		<input type = 'button' id='upload' value='Upload'>
		<div class='data'></div>
	</div>

</body>
</html>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script>

$(document).ready(function(){
	//$(document).on("click","#upload",function(){
		
		$("#upload").bind("click", function () {
            //var base_url = window.location.href();
           // alert(window.location.href());
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
            var _token = $('#_token').val();
            if (regex.test($("#upload_csv").val().toLowerCase())) {
                if (typeof (FileReader) != "undefined") {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        //var table = $("<table />");
                        var data = e.target.result.split('\n');
                        var title = data[0];
                        $.ajax({
                            url:'http://laravel.dev'+'/data_post',
                            data:{
                                'data':data,
                                'title':title,
                                '_token':_token,
                            },
                            type:'post',
                            success : function(ret_data) { 
                                if(ret_data['status']=='success'){
                                    $('.msg_class').text('The data is uploaded successfully');
                                }else{
                                    $('.msg_class').text(ret_data['message']);
                                }
                            }
                        });
                    }
                reader.readAsText($("#upload_csv")[0].files[0]);
                } else {
                    alert("This browser does not support HTML5.");
                }
            } else {
                alert("Please upload a valid CSV file.");
            }
        });

  

});


</script>
